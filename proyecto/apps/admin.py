from django.contrib import admin
from .models import Estadio, Actividad, Asignacion
# Register your models here.
admin.site.register(Estadio)
admin.site.register(Actividad)



class Asignacion_admin(admin.ModelAdmin):
	list_display = ('actividad','usuario')
	list_filter = ('usuario',)
	search_fields = ('usuario__username',)
	

admin.site.register(Asignacion, Asignacion_admin)