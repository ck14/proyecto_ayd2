from django.db import models
from django.contrib.auth.models import User
# Create your models here.
class Estadio(models.Model):
	nombre = models.CharField(max_length=100)
	direccion = models.CharField(max_length=100)
	capacidad = models.CharField(max_length=50)

	def __unicode__(self):
		return self.nombre


class Actividad(models.Model):
	nombre = models.CharField(max_length=100)
	descripcion = models.TextField(max_length=300)
	horario = models.CharField(max_length=50)
	precio = models.DecimalField(max_digits=15,decimal_places=2,default=0)
	estadio = models.ForeignKey(Estadio)
	def __unicode__(self):
		return self.nombre


class Asignacion(models.Model):
	actividad = models.ForeignKey(Actividad)
	usuario = models.ForeignKey(User)

	def __unicode__(self):
		return 'Asignacion_' + str(self.id)





