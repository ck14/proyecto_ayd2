from django.conf.urls import patterns, include, url
from .views import register

urlpatterns = patterns('',
	url(r'^$' , 'django.contrib.auth.views.login',
		{'template_name':'apps/No_Register.html'}, name='login'),

	url(r'^cerrar/$' , 'django.contrib.auth.views.logout_then_login',
		name='logout'),

	url(r'^registrado/$', register.as_view() , name='register'),

	


)
